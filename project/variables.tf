# https://www.terraform.io/docs/language/values/variables.html

variable "project_name" {
  type    = string
  description = "Project display name"
}

variable "project_id" {
  type    = string
  description = "Project ID (must be unique gloablly)"
}

variable "organisation_id" {
  type    = string
  description = "Organisation ID"
}