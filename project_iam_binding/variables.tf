# https://www.terraform.io/docs/language/values/variables.html

variable "project_id" {
  type    = string
  description = "Project ID"
}

variable "role" {
  type    = string
  description = "Role"
}

variable "members" {
  type    = list(string)
  description = "Project ID"
}